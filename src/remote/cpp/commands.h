#include <iostream>
#include <string>
#include <iterator>
#include <vector>
#include <stdbool.h>
#include <unordered_map>
#include <sstream>
#include <fstream>
#include "context.h"
#include "interface.h"

using namespace std;

namespace rov_commands
{
	class Context;
	typedef void (*Command)(Context*, vector<string>);
	class Context
	{
	public:
		rov_context_t *rov_cxt;
		unordered_map<string, string> variables;
		unordered_map<string, Command> commands;
	};


	vector<string> tokenize(string text, string delim);
	void run(rov_context_t *rov_cxt);
	unordered_map<string, Command> init_commands();
	string expand(Context *cxt, string text);
	
	void parse(Context *cxt, vector<string> tokens);

	void parse_motor_pins(Context *cxt, vector<string> tokens);
	void parse_motor_set(Context *cxt, vector<string> tokens);
	void parse_motor_pwm_bounds(Context *cxt, vector<string> tokens);
	void parse_motor_safety_timeout(Context *cxt, vector<string> tokens);

	void parse_set_gripper_pin(Context *cxt, vector<string> tokens);
	void parse_control_gripper(Context *cxt, vector<string> tokens);

	void parse_set_sensor_pin(Context *cxt, vector<string> tokens);
	void parse_sensor_state(Context *cxt, vector<string> tokens);

	void parse_set_camera_pins(Context *cxt, vector<string> tokens);
	void parse_switch_camera(Context *cxt, vector<string> tokens);

	void parse_echo(Context *cxt, vector<string> tokens);

	void parse_comment(Context *cxt, vector<string> tokens);
	void parse_set_variable(Context *cxt, vector<string> tokens);
	void parse_help(Context *cxt, vector<string> tokens);
	void parse_macro(Context *cxt, vector<string> tokens);
	void parse_exit(Context *cxt, vector<string> tokens);

	void unknown(string unknown_command);
}