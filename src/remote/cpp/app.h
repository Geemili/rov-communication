#ifndef _APP_H_
#define _APP_H_

#include <communication.h>
#include <cstdio>
#include <sys/select.h>
#include <serial/serial.h>
#include <stdbool.h>
#include <string>
#include "joystick.h"
#include <pthread.h>
#include "context.h"
#include "interface.h"
#include "commands.h"
#include <ctime>
#include <unistd.h>
#include <iostream>

using std::string;
using std::vector;

namespace rov
{
	void* main_monitor(void* vargp);
	void* main_cli(void* vargp);
	void* main_joystick(void* vargp);
	void* main_interface(void* vargp);
}

#endif