#ifndef _JOYSTICK_H_
#define _JOYSTICK_H_

#include <misc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include "interface.h"
#include <joystick.hh>
#include <string>

#ifndef PI
#define PI 3.14159265
#endif
#ifndef JOYSTICK_REDUCTION_VALUE
#define JOYSTICK_REDUCTION_VALUE 150
#endif

namespace rov_joystick
{
	bool open_input_file(rov_context_t *context);
	void check_controller(rov_context_t *context);
	void move_xy(rov_context_t *context, int x, int y); // Forward, back, left, and right
	void move_z(rov_context_t *context, int value); // Up and down
	void rotate_z(rov_context_t *context, int value); // Rotate left and right
	void balance(rov_context_t *context, int x, int y); // For manually rotating the rov xy axis
}

#endif