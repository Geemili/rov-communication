#include "app.h"


#define BUF_SIZE 16

int main(int argc, char const *argv[])
{
	// Program has three different modes
	// - Serial Monitor (should make stuff easy to see)
	// - Config
	// - Joystick
	// - Buffer

	string joystick_filename = "/dev/input/js0";
	if (argc > 1)
	{
		joystick_filename = argv[1];
	}

	vector<serial::PortInfo> ports = serial::list_ports();
	vector<serial::PortInfo>::iterator iter = ports.begin();

	int i = 0;
	while( iter!=ports.end() )
	{
		serial::PortInfo port_info = *iter++;
		printf("Port %d:\t{%s, %s, %s}\n",
			i,
			port_info.port.c_str(),
			port_info.description.c_str(),
			port_info.hardware_id.c_str());
		i++;
	}

	printf("Type the port number of the serial port you would like to listen to.\n");

	int in;
	scanf("%d", &in);

	serial::Serial *port = new serial::Serial(ports[in].port); // Opens port
	port->setTimeout(100, 100, 100, 100, 100);

	printf("Initializing context\n");
	rov_context_t context;
	{
		context.port = port;

		context.joystick_filename = joystick_filename;

		context.running = true;
	}
	printf("Finished initialization of context\n");

	pthread_t cli_tid;
	pthread_t joystick_tid;

	printf("Beginning cli thread\n");
	pthread_create(&cli_tid, NULL, rov::main_cli, &context);
	printf("Beginning joystick thread\n");
	pthread_create(&joystick_tid, NULL, rov::main_joystick, &context);

	printf("Beginning monitor thread\n");
	rov::main_monitor(&context);
	port->close();

	return 0;
} // main

namespace rov
{
	void* main_monitor(void* vargp)
	{
		rov_context_t* context = (rov_context_t*)vargp;

		printf("[monitor] beginning serial monitor\n");

		while(context->running)
		{
			int available = 0;
			try
			{
				available = context->port->available();
			}
			catch(...)
			{
				std::cout << "[monitor] error checking bytes available" << endl;
			}
			if (available > 0)
			{
				try
				{
					uint8_t buf[2];
					context->port->read(buf, 1);
					buf[1] = 0;
					printf("%s", buf);
				}
				catch(...)
				{
					std::cout << "[monitor] error reading bytes" << endl;
				}
			}
			try
			{
				context->port->waitReadable();
			}
			catch(...)
			{
				std::cout << "[monitor] error waiting to read" << endl;
			}
		}
		return NULL;
	}

	void* main_cli(void* vargp)
	{
		rov_context_t* context = (rov_context_t*)vargp;

		printf("[cli] Beginning ROV Command Line Interface\n");

		rov_commands::run(context);

		context->running = false;
		return NULL;
	}

	void* main_joystick(void* vargp)
	{
		rov_context_t* context = (rov_context_t*)vargp;

		printf("[joystick] initializing joystick interface (%s)\n", context->joystick_filename.c_str());

		if(!rov_joystick::open_input_file(context))
		{
			printf("[joystick] failed to open input stream\n");
			printf("[joystick] ending joystick thread\n");
			return NULL;
		}

		printf("[joystick] beginning joystick loop\n");
		while(context->running)
		{
			rov_joystick::check_controller(context);
			usleep(100);
		}
		return NULL;
	}
}