#ifndef _INTERFACE_H_
#define _INTERFACE_H_

#include "context.h"
#include <communication.h>
#include <ctime>
#include <iostream>
#include <unistd.h>


#ifndef P_X
#define P_X 3 // Motor 3 lies on the positive side of the x-axis
#endif
#ifndef N_X
#define N_X 2 // Motor 2 lies on the negative side of the x-axis
#endif
#ifndef P_Y
#define P_Y 1 // Motor 1 lies on the positive side of the y-axis
#endif
#ifndef N_Y
#define N_Y 4 // Motor 4 lies on the negative side of the y-axis
#endif
#ifndef NUM_MOTORS
#define NUM_MOTORS 16
#endif
#ifndef M_5
#define M_5 5
#endif
#ifndef M_6
#define M_6 6
#endif
#ifndef M_7
#define M_7 7
#endif
#ifndef M_8
#define M_8 8
#endif
#ifndef VALVE_OPENER
#define VALVE_OPENER 9
#endif

/*
	This interface will be designed to make it possible to buffer motor values.
	IF the motor values are not buffered, it is possible that the line will be
	sending too many commands over at once.

	Doesn't seem like that will ever be implemented
*/

namespace rov_interface
{
	/*----- MOTORS -----*/
	void set_pins(rov_context_t *context, int motor_id, int pwm, int left, int right);
	void set_motor(rov_context_t *context, int motor_id, bool left, bool right, int pwm);
	void set_pwm_bounds(rov_context_t *context, int minimum, int maximum);
	void set_safety_timeout(rov_context_t *context, int timeout);

	/*----- GRIPPER -----*/
	void set_gripper_pin(rov_context_t *context, int pin);
	void control_gripper(rov_context_t *context, int pwm);

	/*----- SENSORS ------*/
	void set_sensor_pin(rov_context_t *context, int id, int pin);
	void sensor_state(rov_context_t *context, int id, int state);

	/*----- CAMERAS ------*/
	void set_camera_pins(rov_context_t *context, int pin1, int pin2, int pin3, int pin4);
	void switch_camera(rov_context_t *context, int camera);

	/*----- MISC -----*/
	void echo(rov_context_t *context, int data);
	void send_command(rov_context_t *context, uint8_t *buffer, int length);
}

#endif