#include "interface.h"


namespace rov_interface
{

	/*----- MOTORS -----*/
	void set_pins(rov_context_t *context, int motor_id, int pwm, int left, int right)
	{
		uint8_t buf[] = {
			CMD_SET_MOTOR_PINS,
			((motor_id & 0xF) << 4) | (pwm & 0xF), 
			(left & 0xFF),
			(right & 0xFF)
		};
		send_command(context, buf, 4);
	}

	void set_motor(rov_context_t *context, int motor_id, bool left, bool right, int pwm)
	{
		uint8_t buf[3];
		buf[0] = CMD_CONTROL_MOTOR;
		buf[1] = ((motor_id & 0xF) << 4) | (left ? 0x2 : 0x0) | (right ? 0x1 : 0x0);
		buf[2] = (pwm & 0xFF);
		send_command(context, buf, 3);
	}

	void set_pwm_bounds(rov_context_t *context, int minimum, int maximum)
	{
		uint8_t buf[3];
		buf[0] = CMD_SET_PWM_BOUNDS;
		buf[1] = (minimum & 0xFF);
		buf[2] = (maximum & 0xFF);
		send_command(context, buf, 3);
	}

	void set_safety_timeout(rov_context_t *context, int timeout)
	{
		uint8_t buf[3];
		buf[0] = CMD_SET_SAFETY_TIMEOUT;
		buf[1] = (timeout & 0xFF);
		buf[2] = ((timeout >> 4) & 0xFF);
		send_command(context, buf, 3);
	}

	/*----- GRIPPER -----*/
	void set_gripper_pin(rov_context_t *context, int pin)
	{
		uint8_t buf[2];
		buf[0] = CMD_SET_GRIPPER_PIN;
		buf[1] = (pin & 0xFF);
		send_command(context, buf, 2);
	}

	void control_gripper(rov_context_t *context, int pwm)
	{
		uint8_t buf[2];
		buf[0] = CMD_CONTROL_GRIPPER;
		buf[1] = (pwm & 0xFF);
		send_command(context, buf, 2);
	}

	/*----- SENSORS -----*/
	void set_sensor_pin(rov_context_t *context, int id, int pin)
	{
		uint8_t buf[3];
		buf[0] = CMD_SET_SENSOR_PIN;
		buf[1] = (id & 0xFF);
		buf[2] = (pin & 0xFF);
		send_command(context, buf, 3);
	}

	void sensor_state(rov_context_t *context, int id, int state)
	{
		uint8_t buf[3];
		buf[0] = CMD_SENSOR_STATE;
		buf[1] = (id & 0xFF);
		buf[2] = (state & 0xFF);
		send_command(context, buf, 3);
	}


	/*----- CAMERAS -----*/
	void set_camera_pins(rov_context_t *context, int pin1, int pin2, int pin3, int pin4)
	{
		uint8_t buf[5];
		buf[0] = CMD_SET_CAMERA_PINS;
		buf[1] = (pin1 & 0xFF);
		buf[2] = (pin2 & 0xFF);
		buf[3] = (pin3 & 0xFF);
		buf[4] = (pin4 & 0xFF);
		send_command(context, buf, 5);
	}

	void switch_camera(rov_context_t *context, int camera)
	{
		uint8_t buf[2];
		buf[0] = CMD_SWITCH_CAMERA;
		buf[1] = (camera & 0xFF);
		send_command(context, buf, 2);
	}

	/*----- MISC -----*/
	void echo(rov_context_t *context, int data)
	{
		uint8_t buf[2];
		buf[0] = CMD_ECHO;
		buf[1] = (data & 0xFF);
		send_command(context, buf, 2);
	}


	void send_command(rov_context_t *context, uint8_t *buffer, int length)
	{
		try
		{
			context->port->write(buffer, length);
			context->port->flush();
		}
		catch(serial::IOException e)
		{
			printf("Error occured while writing to port.\n{ ");
			for (int i = 0; i < length; ++i)
			{
				printf("%d ", buffer[i]);
			}
			printf("}\n");
			std::cout << e.what();
		}
		catch(...)
		{
			printf("Error occured while writing to port.\n{ ");
			for (int i = 0; i < length; ++i)
			{
				printf("%d ", buffer[i]);
			}
			printf("}\n");
		}
	}

}