#include "commands.h"

namespace rov_commands
{
	vector<string> tokenize(string text, string delim)
	{
		vector<string> tokens = vector<string>();
		int i = 0;
		int start = 0;
		int end = text.find(delim);
		while (end != string::npos)
		{
			string to_insert = text.substr(start, end - start);
			if (!to_insert.empty())
			{
				tokens.insert(tokens.begin()+i, to_insert);
				i++;
			}
			start = end + delim.length();
			end = text.find(delim, start);
		}
		tokens.insert(tokens.begin()+i, text.substr(start, end));
		return tokens;
	}

	void run(rov_context_t *rov_cxt)
	{
		Context *cxt = new Context();
		cxt->rov_cxt = rov_cxt;
		cxt->variables = unordered_map<string, string>();
		cxt->commands = init_commands();

		string input;
		cxt->variables["running"] = "true";
		while(rov_cxt->running)
		{
			cout << "rov$ ";
			if (!getline(cin, input))
			{
				cout << "exit" << endl;
				break;
			}
			string expanded_input = expand(cxt, input);
			vector<string> tokens = tokenize(expanded_input, " ");
			parse(cxt, tokens);
			if (cxt->variables["running"]!="true")
			{
				break;
			}
		}
		rov_cxt->running = false;
	}

	string expand(Context *cxt, string text_param)
	{
		string text;
		text = text_param;
		int end;
		int start = text.find("$");
		string var;
		unordered_map<string, string>::const_iterator got;
		bool has_value;
		while (start != string::npos)
		{
			end = text.find(" ", start);
			if (end == string::npos)
			{
				end = text.length();
			}
			var = text.substr(start+1, end - start);
			got = cxt->variables.find(var);
			has_value = (got != cxt->variables.end());
			if (has_value)
			{
				text.replace(start, end-start, (*got).second);
			}
			else
			{
				text.replace(start, end, "");
			}
			start = text.find("$", start);
		}
		return text;
	}

	void parse(Context *cxt, vector<string> tokens)
	{
		if (tokens.size() <=0 || (tokens.size() <= 1 && tokens[0]==""))
			return; // No command was entered
		string command = tokens[0];
		unordered_map<string, Command>::const_iterator got = cxt->commands.find(command);
		if (got != cxt->commands.end())
		{
			got->second(cxt, tokens);
		} 
		else
		{
			unknown(command);
		}
	}


	/*     MOTORS     */

	void parse_motor_pins(Context *cxt, vector<string> tokens)
	{
		int motor_id = stoi(tokens[1]);
		int a = stoi(tokens[2]);
		int b = stoi(tokens[3]);
		int pwm = stoi(tokens[4]);
		printf("%d\t%d\t%d\t%d\n", motor_id, a, b, pwm);
		rov_interface::set_pins(cxt->rov_cxt, motor_id, pwm, a, b);
	}

	void parse_motor_set(Context *cxt, vector<string> tokens)
	{
		int motor_id = stoi(tokens[1]);
		int a = stoi(tokens[2]);
		int b = stoi(tokens[3]);
		int pwm = stoi(tokens[4]);
		rov_interface::set_motor(cxt->rov_cxt, motor_id, a, b, pwm);
	}

	void parse_motor_pwm_bounds(Context *cxt, vector<string> tokens)
	{
		int lower = stoi(tokens[1]);
		int higher = stoi(tokens[2]);
		rov_interface::set_pwm_bounds(cxt->rov_cxt, lower, higher);
	}

	void parse_motor_safety_timeout(Context *cxt, vector<string> tokens)
	{
		int timeout = stoi(tokens[1]);
		rov_interface::set_safety_timeout(cxt->rov_cxt, timeout);
	}


	/*     GRIPPERS     */
	void parse_set_gripper_pin(Context *cxt, vector<string> tokens)
	{
		int pin = stoi(tokens[1]);
		rov_interface::set_gripper_pin(cxt->rov_cxt, pin);
	}

	void parse_control_gripper(Context *cxt, vector<string> tokens)
	{
		int pwm = stoi(tokens[1]);
		rov_interface::control_gripper(cxt->rov_cxt, pwm);
	}



	/*     SENSORS     */
	void parse_set_sensor_pin(Context *cxt, vector<string> tokens)
	{
		int sensor_id = stoi(tokens[1]);
		int pin = stoi(tokens[2]);
		rov_interface::set_sensor_pin(cxt->rov_cxt, sensor_id, pin);
	}

	void parse_sensor_state(Context *cxt, vector<string> tokens)
	{
		int sensor_id = stoi(tokens[1]);
		int state = stoi(tokens[2]);
		rov_interface::sensor_state(cxt->rov_cxt, sensor_id, state);
	}


	/*     CAMERAS     */
	void parse_set_camera_pins(Context *cxt, vector<string> tokens)
	{
		int pin1 = stoi(tokens[1]);
		int pin2 = stoi(tokens[2]);
		int pin3 = stoi(tokens[3]);
		int pin4 = stoi(tokens[4]);
		rov_interface::set_camera_pins(cxt->rov_cxt, pin1, pin2, pin3, pin4);
	}

	void parse_switch_camera(Context *cxt, vector<string> tokens)
	{
		int camera = stoi(tokens[1]);
		rov_interface::switch_camera(cxt->rov_cxt, camera);
	}


	/*     MISC     */

	void parse_echo(Context *cxt, vector<string> tokens)
	{
		if(tokens.size() <= 1) return;
		const char *text = tokens[1].c_str();
		for (int i = 0; i < tokens[1].length(); ++i)
		{
			rov_interface::echo(cxt->rov_cxt, (int) text[i]);
		}
	}

	void parse_comment(Context *cxt, vector<string> tokens)
	{
		// Do nothing; command for human readability only
	}

	void parse_set_variable(Context *cxt, vector<string> tokens)
	{
		if (tokens.size() < 3)
		{
			cout << "Not enough args" << endl;
			return;
		}
		if (tokens[1] == "var")
		{
			cxt->variables[tokens[2]] = tokens.size()>3 ? tokens[3] : "";
		}
		if (tokens[1] == "notset")
		{
			unordered_map<string, string>::const_iterator got = cxt->variables.find(tokens[2]);
			if (got == cxt->variables.end() || (*got).second.empty())
			{
				cxt->variables[tokens[2]] = tokens.size()>3 ? tokens[3] : "";
			}
		}
		else
		{
			cout << "Invalid arg" << endl;
		}
	}

	void parse_help(Context *cxt, vector<string> tokens)
	{
		for (auto kv : cxt->commands)
		{
			cout << kv.first << endl;
		}
	}

	void parse_macro(Context *cxt, vector<string> tokens)
	{
		if (tokens.size()<=1) return;
		for (int i = 1; i < tokens.size(); ++i)
		{
			cxt->variables[to_string(i-1)] = tokens[i];
		}

		string filename = tokens[1];
		ifstream in(filename);
		stringstream sstr;
		sstr << in.rdbuf();
		string text = sstr.str();

		vector<string> lines = tokenize(text, "\n");
		for (vector<string>::iterator i = lines.begin(); i != lines.end(); ++i)
		{
			string expanded_line = expand(cxt, *i);
			vector<string> m_tokens = tokenize(expanded_line, " ");
			parse(cxt, m_tokens);
		}

		for (int i = 1; i < tokens.size(); ++i)
		{
			cxt->variables[to_string(i)] = "";
		}
	}

	void parse_exit(Context *cxt, vector<string> tokens)
	{
		cxt->variables["running"] = "false";
	}

	void unknown(string unknown_command)
	{
		cout << "Uknown command '" << unknown_command << "'. Type 'help' for help." << endl;
	}

	unordered_map<string, Command> init_commands()
	{
		unordered_map<string, Command> command = unordered_map<string, Command>();

		// Motors
		command["motor_pins"] = &parse_motor_pins;
		command["motor_set"] = &parse_motor_set;
		command["motor_set_pwm_bounds"] = &parse_motor_pwm_bounds;
		command["motor_set_safety_timeout"] = &parse_motor_safety_timeout;

		// Gripper
		command["set_gripper_pin"] = &parse_set_gripper_pin;
		command["control_gripper"] = &parse_control_gripper;

		// Sensors
		command["set_sensor_pin"] = &parse_set_sensor_pin;
		command["sensor_state"] = &parse_sensor_state;

		// Camera
		command["set_camera_pins"] = &parse_set_camera_pins;
		command["switch_camera"] = &parse_switch_camera;

		// Misc
		command["echo"] = &parse_echo;
		command["set"] = &parse_set_variable;
		command["help"] = &parse_help;
		command["macro"] = &parse_macro;
		command["exit"] = &parse_exit;
		command["#"] = &parse_comment;
		return command;
	}
}