#ifndef _CONTEXT_H_
#define _CONTEXT_H_

#include <pthread.h>
#include <stdbool.h>
#include <serial/serial.h>
#include <string>
#include <joystick.hh>

typedef struct rov_context_t
{
	serial::Serial *port;

	std::string joystick_filename;
	Joystick *joystick;

	bool running;
} rov_context_t;

#endif