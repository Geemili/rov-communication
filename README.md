Lakeview ROV arduino software.


# To run the remote

Before running the remote, make sure that you have downloaded all the submodules. This can be done by running the following commands:
```
git submodule init
git submodules update
```

Once you have that, you can build and run the software.
```
./gradlew remoteExecutable
./build/binaries/remoteExecutable/remote
```

# To upload to the robot

This has not yet been implemented.